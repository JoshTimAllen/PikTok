﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PikTok.Models {
    public class SearchViewModel : IMultiParametersInOneModel {
        public UserAccount userAccount { get; set; }
        public List<ProfileModel> searchResults { get; set; }
    }
}
