﻿var socket = new WebSocket("ws://localhost:1000/chat");

socket.onopen = function (e) {
    // alert("[open] Connection established");
    // alert("Sending to server");
};

socket.onmessage = function (event) {
    console.log("socket message", event.data);
    var messageData = JSON.parse(event.data);
    if (messageData.packetType == 0) {
        UpdateChats(messageData);
    }
};

socket.onclose = function (event) {
    if (event.wasClean) {
        //alert(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
    } else {
        // e.g. server process killed or network down
        // event.code is usually 1006 in this case
    }
};

socket.onerror = function (error) {
    //alert(`[error] ${error.message}`);
};

var messageType = {

}

function MessagePacket () {
    this.packetType = 0;
    this.externalUserId = "";
    this.senderExternalUserId = "";
    this.message = "";
    this.profilePictureUrl = "";
}