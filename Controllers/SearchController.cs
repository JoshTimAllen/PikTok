﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using PikTok.Components;
using PikTok.Models;
using PikTok.Services;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text.Json;
using System.Threading.Tasks;

namespace PikTok.Controllers {
    [ApiController]
    [Route("[controller]")]
    public class SearchController : Controller {
        IWebHostEnvironment webHostEnvironment;
        private readonly IHtmlHelper htmlHelper;

        public SearchController(AuthService authService, DatabaseService databaseService) {
            this.authService = authService;
            this.databaseService = databaseService;
        }
        public AuthService authService { get; }
        private readonly DatabaseService databaseService;
        [HttpGet]
        public async Task<IActionResult> Search(string queryString) {
            //Checks if cookies contains Token
            if (!Request.Cookies.ContainsKey("Token")) { return View("cta"); }
            UserAccount userAccount = await authService.GetUser(Request.Cookies);
            var list = await databaseService.Search(queryString);
            return View("Search", new SearchViewModel() {
                userAccount = userAccount,
                searchResults = list
        });
        }
    }
}